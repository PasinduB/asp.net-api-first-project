﻿using DogReview.Interfaces;
using DogReview.Models;

namespace DogReview.Repository
{
    public class CountryRepository : ICountryRepository
    {

        private DataContext _context;

        public CountryRepository(DataContext context)
        {
            _context = context;
        }
        public ICollection<Country> GetCountries()
        {
            return _context.Countries.ToList();
        }

        public Country GetCountry(int id)
        {
            return _context.Countries.Where(c=>c.CountryId==id).FirstOrDefault();
        }
    }
}
