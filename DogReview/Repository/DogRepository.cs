﻿using DogReview.Interfaces;
using DogReview.Models;

namespace DogReview.Repository
{
    public class DogRepository : IDogRepository

        
    {
        private readonly DataContext _context;
        public DogRepository(DataContext context) { 
        _context = context;
        }

        public Dog GetDog(int dogId)
        {
            return _context.Dog.Where(d => d.DogId == dogId).FirstOrDefault();
        }

        public Dog GetDog(string dogName)
        {
            return _context.Dog.Where(d => d.DogName == dogName).FirstOrDefault();
        }

        public decimal GetDogRating(int dogId)
        {
            var review = _context.Reviews.Where(d => d.Dog.DogId == dogId);

            if(review.Count() <=0)
                return 0;
            return ((decimal)review.Sum(r => r.Dog.DogId) / review.Count());


        }

        public ICollection<Dog> GetDogs()
        {
            return _context.Dog.OrderBy(d=>d.DogId).ToList();
        }

        public bool DogExsist(int dogId)
        {
            return _context.Dog.Any(d=>d.DogId==dogId);
        }

        
    }
}
