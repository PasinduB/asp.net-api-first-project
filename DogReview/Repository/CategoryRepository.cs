﻿using DogReview.Interfaces;
using DogReview.Models;

namespace DogReview.Repository
{
    public class CategoryRepository : ICategoryRepository
        
    {
        private DataContext _context;

        public CategoryRepository(DataContext context)
        {
            _context = context;
        }

        public bool CategoryExists(int id)
        {
            return _context.Categories.Any(c => c.CategoryId == id);
        }

        public ICollection<Category> GetCategories()
        {
            return _context.Categories.ToList();
        }

        public Category GetCategory(int id)
        {
            return _context.Categories.Where(c => c.CategoryId == id).FirstOrDefault();
        }

        public ICollection<Dog> GetDogsByCategory(int id)
        {
            return _context.DogCategories.Where(c=>c.CategoryId == id).Select(d=>d.Dog).ToList();

        }
    }
}
