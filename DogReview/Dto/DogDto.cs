﻿namespace DogReview.Dto
{
    public class DogDto
    {
        public int DogId { get; set; }
        public string DogName { get; set; }
        public DateTime DogBirthDate { get; set; }
    }
}
