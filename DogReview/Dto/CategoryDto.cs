﻿using DogReview.Models;

namespace DogReview.Dto
{
    public class CategoryDto
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        
    }
}
