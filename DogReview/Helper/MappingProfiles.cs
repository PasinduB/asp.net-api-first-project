﻿using AutoMapper;
using DogReview.Dto;
using DogReview.Models;

namespace DogReview.Helper
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Dog, DogDto>();
        }
    }
}