﻿using AutoMapper;
using DogReview.Dto;
using DogReview.Interfaces;
using DogReview.Models;
using DogReview.Repository;
using Microsoft.AspNetCore.Mvc;

namespace DogReview.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("api/[controller]")]
    [ApiController]

    
    public class CategoryController : Controller
    {
        private ICategoryRepository _categoryRepository;
        private IMapper _mapper;
        

        public CategoryController( ICategoryRepository categoryRepository , IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;

        }
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Category>))]
        public IActionResult GetCategories()
        {
            var categories = _mapper.Map<List<CategoryDto>>(_categoryRepository.GetCategories);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);



            return Ok(categories);
        }

        [HttpGet("{CategoryId}")]
        [ProducesResponseType(200, Type = typeof(Category))]
        [ProducesResponseType(400)]
        public IActionResult GetCategory(int CategoryId)
        {

            if (_categoryRepository.CategoryExists(CategoryId))
            {
                var category = _mapper.Map<CategoryDto>(_categoryRepository.GetCategory(CategoryId));

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                return Ok(category);
            }

            return NotFound();
        }

        
    }
}
