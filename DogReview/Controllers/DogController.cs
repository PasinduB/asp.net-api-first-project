﻿using AutoMapper;
using DogReview.Dto;
using DogReview.Interfaces;
using DogReview.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;


namespace DogReview.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("api/[controller]")]
    [ApiController]
    public class DogController : Controller
    {
        private readonly IDogRepository _dogRepository;
        private readonly IMapper _mapper;

        public DogController (IDogRepository dogRepository , IMapper mapper)
        {
            _dogRepository = dogRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(200,Type=typeof(IEnumerable<Dog>))]
        public IActionResult GetDogs()
        {
            var dogs =_mapper.Map<List<DogDto>>(_dogRepository.GetDogs());

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            

            return Ok(dogs);
        }

        [HttpGet("{DogId}")]
        [ProducesResponseType (200,Type=typeof(Dog))]
        [ProducesResponseType (400)]
        public IActionResult GetDog(int DogId)
        {

            if (_dogRepository.DogExsist(DogId))
{
                var dog = _mapper.Map<DogDto>(_dogRepository.GetDog(DogId));

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                return Ok(dog);
            }

            return NotFound();
        }
        [HttpGet("{DogId}/rating")]
        [ProducesResponseType(200, Type = typeof(Dog))]
        [ProducesResponseType(400)]
        public IActionResult GetDogRating(int DogId)
        {

            if (_dogRepository.DogExsist(DogId))
            {
                var rating = _dogRepository.GetDogRating(DogId);

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                return Ok(rating);
            }

            return NotFound();
        }
    }
}
