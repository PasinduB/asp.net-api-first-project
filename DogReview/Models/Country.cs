﻿namespace DogReview.Models
{
    public class Country
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }

        public ICollection<Owner> Owner { get; set; }
    }

}
