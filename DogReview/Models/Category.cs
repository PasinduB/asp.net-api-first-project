﻿namespace DogReview.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public ICollection<DogCategory> DogCategories {  get; set; }
    }
}

