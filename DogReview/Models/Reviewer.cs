﻿namespace DogReview.Models
{
    public class Reviewer
    {
        public int ReviewerId { get; set; }
        public string ReviewerFirstName { get; set;}
        public string ReviewerLastName { get; set;}
        public ICollection<Review> Reviews { get; set;}
    }
}
