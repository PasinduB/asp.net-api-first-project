﻿namespace DogReview.Models
{
    public class Owner
    {
        public int OwnerId { get; set; }
        public string OwnerName { get; set; }

        public ICollection<DogOwner> DogOwners { get; set; }
        
    }
}
