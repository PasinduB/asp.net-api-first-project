﻿namespace DogReview.Models
{
    public class Dog
    {
        public int DogId { get; set; }
        public string DogName { get; set; }
        public DateTime DogBirthDate { get; set; }
        public ICollection<Review> Reviews { get; set; }

        public ICollection<DogOwner> DogOwners { get; set; }
        public ICollection<DogCategory> DogCategories { get; set; }
    }
}
