﻿namespace DogReview.Models
{
    public class Review
    {
        public int ReviewId { get; set; }
        public string ReviewTitle { get; set;}
        public string ReviewText { get; set;}

        public Reviewer Reviewer { get; set; }
        public Dog Dog { get; set; }
    }
}
