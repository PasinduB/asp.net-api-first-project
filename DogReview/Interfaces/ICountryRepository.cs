﻿using DogReview.Models;

namespace DogReview.Interfaces
{
    public interface ICountryRepository
    {
       ICollection <Country> GetCountries();
        Country GetCountry (int id);


    }
}
