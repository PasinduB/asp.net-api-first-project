﻿using DogReview.Models;

namespace DogReview.Interfaces
{
    public interface IDogRepository
    {
       

        ICollection<Dog> GetDogs();

        Dog GetDog(int dogId);
        Dog GetDog (string DogName);
        decimal GetDogRating(int dogId);
        bool DogExsist(int dogId);

        bool CreateDog(int ownerId, int categoryId, Dog dog);
    }
}
