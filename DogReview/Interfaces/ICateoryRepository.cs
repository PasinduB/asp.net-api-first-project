﻿using DogReview.Models;

namespace DogReview.Interfaces
{
    public interface ICategoryRepository
    {
        ICollection<Category> GetCategories();
        Category GetCategory(int id);
        ICollection<Dog> GetDogsByCategory(int categoryId);
        bool CategoryExists(int id);
    }
}
